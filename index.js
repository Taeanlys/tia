let _ = require('underscore')
let config = require('./config')

let nlu = config.nlu;

let main = function (url) {
    var parameters = {
        'url': url,
        'features': {
            'keywords': {
                'sentiment': true,
                'emotion': true
            },
            'entities': {
                'emotion': true,
                'sentiment': true
            },
        }
    };
    let result = [];
    var P_nlu = new Promise((resolve, reject) => {
        nlu.analyze(parameters, function (err, response) {
            if (err)
                console.error('error:', err);
            else
                result = response;
            resolve(result)
        })
    }).then(function (result) {
        let resulta = [];
        let emotions = {}

        _.each(result.keywords, function(el){
            let keys = _.keys(el.emotion)
            _.each(keys, function (em) {
                if (!emotions[em]) {emotions[em] = 0}
                emotions[em] += el.emotion[em]
            })
        })
        let keywordNumber = result.keywords.length  
        let keys = _.keys(emotions)      
        _.each(keys, function (em){
            emotions[em] = emotions[em] / keywordNumber;
        })
        
        resulta.emotions = emotions;
        console.log(resulta.emotions)
    })
}

main("kevin.riou.pro/who");